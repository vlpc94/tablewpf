﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows;

namespace CustomFunctions
{
    /// <summary>
    /// Реализация быстрой сортировки.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class QuickSort<T>
    {
        
        /// <summary>
        /// Количество потоков для сортировки.
        /// </summary>
        private static int _countThreads = Environment.ProcessorCount*12; // 12 - выбрано эксперементальным путем.

        /// <summary>
        /// Запуск быстрой сортировки.
        /// </summary>
        /// <param name="mas"></param>
        /// <param name="order"></param>
        /// <param name="comparer"></param>
        /// <returns></returns>
        public static IEnumerable<T> StartSort(T[] mas, SortOrder order, Func<T, T, int> comparer)
        {
            int sortOrderIndex = order == SortOrder.Ascending ? 1 : -1;
            mas = ParallelQuickSorting(mas, sortOrderIndex, comparer, _countThreads);
            return mas;
        }

        /// <summary>
        /// Быстрая сортировка
        /// </summary>
        /// <param name="mas"></param>
        /// <param name="firstIndex"></param>
        /// <param name="lastIndex"></param>
        /// <param name="sortOrderIndex"></param>
        /// <param name="comparer">Функция сравнения.</param>
        /// <returns></returns>
        private static T[] QuickSorting(T[] mas, int firstIndex, int lastIndex, int sortOrderIndex,
            Func<T, T, int> comparer)
        {
            int left = firstIndex;
            int right = lastIndex;

            T midElement = mas[(firstIndex + lastIndex) >> 1];

            do
            {
                while (comparer(mas[left], midElement)*sortOrderIndex == -1) left++;
                while (comparer(midElement, mas[right])*sortOrderIndex == -1) right--;
                if (left <= right)
                {
                    T temp = mas[left];
                    mas[left] = mas[right];
                    mas[right] = temp;
                    left++;
                    right--;
                }
            } while (left < right);

            if (firstIndex < right) QuickSorting(mas, firstIndex, right, sortOrderIndex, comparer);
            if (left < lastIndex) QuickSorting(mas, left, lastIndex, sortOrderIndex, comparer);
            
            return mas;
        }

        /// <summary>
        /// Быстрая сортировка в несколько потоков
        /// </summary>
        /// <param name="mas"></param>
        /// <param name="sortOrderIndex">Порядок сортировки</param>
        /// <param name="comparer">Функция сравнения.</param>
        /// <param name="countCurrent">Количество потоков сортировки.</param>
        /// <returns></returns>
        private static T[] ParallelQuickSorting(T[] mas, int sortOrderIndex,
            Func<T, T, int> comparer, int countCurrent=1)
        {
            List<T[]> sortedParts=new List<T[]>(countCurrent);
            int length = mas.Length / countCurrent;
            WaitHandle[] sortedOperationsStatus=new WaitHandle[countCurrent];

            Action<int, int, int> sortAction = (index, prevIndex, partLength) =>
            {
                T[] masPart = new T[partLength];
                Array.Copy(mas, prevIndex, masPart, 0, partLength);
                sortedParts.Add(masPart);
                ManualResetEvent sortedStatus = new ManualResetEvent(false);
                sortedOperationsStatus[index] = sortedStatus;
                ThreadPool.QueueUserWorkItem(
                    x =>
                    {
                        masPart = QuickSorting(masPart, 0, masPart.Length - 1, sortOrderIndex, comparer);
                        sortedStatus.Set();
                    });
            };

            int lastSortActionIndex = countCurrent - 1;
            for (int i = 0; i < lastSortActionIndex; i++)
            {
                sortAction(i, i*length, length);
            }

            sortAction(lastSortActionIndex, lastSortActionIndex * length, length + mas.Length % countCurrent);

            WaitHandle.WaitAll(sortedOperationsStatus);
            
            int indexMerge = 0;
            while (sortedParts.Count > 1)
            {
                if (indexMerge >= sortedParts.Count || indexMerge + 1 >= sortedParts.Count) indexMerge = 0;

                var merged = Sort<T>.MergeSort(sortedParts[indexMerge], sortedParts[indexMerge + 1], comparer, sortOrderIndex);
                sortedParts.RemoveAt(indexMerge);
                sortedParts[indexMerge++] = merged;
            }
            
            return sortedParts.First();
        }
        
    }
}
