﻿using System;
using System.Collections;

namespace CustomFunctions
{
    /// <summary>
    /// Сущность для сравнения обьектов.
    /// </summary>
    public class CollectionElementEqualityComparer : IEqualityComparer
    {
        private readonly Func<object, object, bool> _equals;
        private readonly Func<object, int> _getHashCode;

        public CollectionElementEqualityComparer()
        {
            _equals = Equals;
            _getHashCode = GetHashCode;
        }

        public CollectionElementEqualityComparer(Func<object, object, bool> equals,
            Func<object, int> getHashCode)
        {
            _equals = equals;
            _getHashCode = getHashCode;
        }

        public bool Equals(object x, object y)
        {
            return _equals(x, y);
        }

        public int GetHashCode(object obj)
        {
            return _getHashCode(obj);
        }
    }
}
