﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomFunctions
{
    public class Sort<T>
    {
        /// <summary>
        /// Слияние отсортированых массивов.
        /// </summary>
        /// <param name="masLeft"></param>
        /// <param name="masRight"></param>
        /// <param name="comparer"></param>
        /// <param name="sortOrderIndex"></param>
        /// <returns></returns>
        public static T[] MergeSort(T[] masLeft, T[] masRight, Func<T, T, int> comparer, int sortOrderIndex)
        {
            int index1 = 0;
            int index2 = 0;
            T[] mas = new T[masLeft.Length + masRight.Length];

            for (int i = 0; i < mas.Length; i++)
            {
                if (index1 < masLeft.Length && index2 < masRight.Length)
                {
                    mas[i] = comparer(masLeft[index1], masRight[index2]) * sortOrderIndex == 1 ? masRight[index2++] : masLeft[index1++];
                }
                else
                {
                    if (index1 < masLeft.Length)
                        mas[i] = masLeft[index1++];
                    else
                    {
                        mas[i] = masRight[index2++];
                    }
                }
            }

            return mas;
        }
    }
}
