﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomFunctions
{
    /// <summary>
    /// Хеш-таблица с решением колизий.
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class HashTableWithResolveCollisions<TKey, TValue>
    {
        private Dictionary<TKey, List<TValue>> _table;

        public HashTableWithResolveCollisions()
        {
            _table=new Dictionary<TKey, List<TValue>>();
        }

        /// <summary>
        /// Добавление обьекта в таблицу.
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <param name="value">Значение</param>
        public void Add(TKey key, TValue value)
        {
            List<TValue> listValues;
            if (_table.TryGetValue(key, out listValues))
            {
                listValues.Add(value);
            }
            else
            {
                _table.Add(key, new List<TValue>() {value});
            }
        }

        /// <summary>
        /// Извлекает первое найденое значение по ключу.
        /// В случае его отсутствия возвращает false.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool TryPullValue(TKey key, out TValue value)
        {
            value = default(TValue);
            try
            {
                List<TValue> listValues;
                if (_table.TryGetValue(key, out listValues))
                {
                    if (listValues.Any())
                    {
                        value = listValues.First();
                        if (listValues.Count == 1) _table.Remove(key);
                        else listValues.RemoveAt(0);
                    }
                    
                    return true;

                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Возвращает колекцию значений найденых по ключу.
        /// В случае его отсутствия возвращает false.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public bool TryGetValue(TKey key, out List<TValue> values)
        {
            List<TValue> listValues;
            if (_table.TryGetValue(key, out listValues))
            {
                
                    values = listValues;
                    return true;

            }
            values = new List<TValue>();
            return false;
        }

        /// <summary>
        /// Возвращает первое найденое значение по ключу.
        /// В случае его отсутствия возвращает false.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool TryGetFirstValue(TKey key, out TValue value)
        {
            List<TValue> listValues;
            if (_table.TryGetValue(key, out listValues))
            {
                if (listValues.Any())
                {
                    value = listValues.First();
                    return true;
                }

            }
            value = default(TValue);
            return false;
        }
    }
}
