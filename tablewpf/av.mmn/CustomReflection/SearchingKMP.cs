﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomFunctions
{
    /// <summary>
    /// Алгоритм поиска подстроки Кнута-Морриса-Пратта
    /// </summary>
    public class SearchingKMP
    {
        public static int[] PreficsFunc(string x)
        {
            //Инициализация массива-результата длиной X
            int[] res = new int[x.Length];
            int i = 0;
            int j = -1;
            res[0] = -1;
            //Вычисление префикс-функции
            while (i < x.Length - 1)
            {
                while ((j >= 0) && (x[j] != x[i]))
                    j = res[j];
                i++;
                j++;
                if (x[i] == x[j])
                    res[i] = res[j];
                else
                    res[i] = j;
            }
            return res;
        }

        /// <summary>
        /// Функция поиска подстроки алгоритмом Кнута-Морриса-Пратта
        /// </summary>
        /// <param name="x"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool KMP(string x, string s)
        {
            string nom = ""; //Объявление строки с номерами позиций
            if (x.Length > s.Length) return false; //Возвращает 0 поиск если образец больше исходной строки
            //Вызов префикс-функции
            int[] d = PreficsFunc(x);
            int i = 0, j;
            while (i < s.Length)
            {
                for (j = 0; (i < s.Length) && (j < x.Length); i++, j++)
                    while ((j >= 0) && (x[j] != s[i]))
                        j = d[j];
                if (j == x.Length)
                    nom = nom + Convert.ToString(i - j) + ", ";
            }
            
            return nom.Length>0; //Возвращение результата поиска
        }
    }
}
