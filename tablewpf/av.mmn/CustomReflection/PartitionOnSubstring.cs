﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomFunctions
{
    //Тестовый клас. Можно удалять.
    public class PartitionWord
    {
        /// <summary>
        /// Разбиение слова на все возможные подстроки.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static List<string> PartitionOnSubstring(string str)
        {
            return PartitionOnSubstring(str, false);
        }

        private static List<string> PartitionOnSubstring(string str, bool reverse)
        {
            List<string> substrings=new List<string>();

            if (str.Length > 0)
            {
                if (reverse)
                {
                    for (int i = str.Length - 1; i >= 0; i--)
                    {
                        substrings.Add(str.Substring(i, str.Length - i));
                    }
                    substrings.AddRange(PartitionOnSubstring(str.Substring(0, str.Length - 1), !reverse));
                }
                else
                {
                    for (int i = 0; i < str.Length; i++)
                    {
                        substrings.Add(str.Substring(0, i + 1));
                    }
                    substrings.AddRange(PartitionOnSubstring(str.Substring(1, str.Length - 1), !reverse));
                }
            }

            return substrings;
        }

    }
}
