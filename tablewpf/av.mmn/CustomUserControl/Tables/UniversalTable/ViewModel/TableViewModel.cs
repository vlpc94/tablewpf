﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Web.UI.WebControls;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Navigation;
using CustomFunctions;
using CustomFunctions.Extensions;
using Expression = System.Linq.Expressions.Expression;

namespace CustomUserControl.Tables.UniversalTable
{
    public class TableViewModel
    {
        private ObservableCollection<GridViewColumnDescription> _columns;
        private ObservableCollectionWithMaxLenElement<object> _rows;
        private IEnumerable<object> _dataSource;
        private bool _showNameHeaderByAttribute;
        private List<PropertyInfo> _sortPropertiesPriority;
        private Action _sortAction;
        private Action _searchAction;
        private Action _changeCallBackAction;
        private Func<Object, Object, int> _rowCompareFunc;
        private Dictionary<string, HashTableWithResolveCollisions<long, int>> _hashTableFields;
        private Dictionary<string, HashTableWithResolveCollisions<long, int>> _hashTableSubstringFields;

        /// <summary>
        /// Колекция данных для привязки.
        /// </summary>
        public IEnumerable<object> DataSource
        {
            get
            {
                return _dataSource;
            }
            set
            {
                _dataSource = value;
                _columns = new ObservableCollection<GridViewColumnDescription>();
                var col = _dataSource?.FirstOrDefault(p => p != null);
                if (col != null)
                {
                    _rows = new ObservableCollectionWithMaxLenElement<object>(_dataSource);

                    List<string> columnsBindingName = new List<string>(col.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).Select(p => p.Name));

                    foreach (var nameProperty in columnsBindingName)
                    {
                        var atr = col.GetType()
                            .GetProperty(nameProperty, BindingFlags.Public | BindingFlags.Instance)
                            .GetCustomAttributes(typeof(InfoFieldAttribute), true);
                        string showedNameHeader = atr.Any() && _showNameHeaderByAttribute ? ((InfoFieldAttribute)atr.First()).ShowName : nameProperty;
                        var column = new GridViewColumnDescription(nameProperty, _sortAction, _searchAction,
                            showedNameHeader,
                            Rows.GetMaxLengthColumn(showedNameHeader)*
                            (int) Unit.Pixel(showedNameHeader.Length + 2).Value,
                            false, true);
                        column.SearchParametrChange += (sender, args) => { _searchResult = null; };
                        _columns.Add(column);
                    }
                }
            }
        }

        /// <summary>
        /// Колекция столбцов.
        /// </summary>
        public ObservableCollection<GridViewColumnDescription> Columns
        {
            get { return _columns; }
        }

        /// <summary>
        /// Колекция строк для отображения.
        /// </summary>
        public ObservableCollectionWithMaxLenElement<Object> Rows
        {
            get
            {
                return _rows;
            }
        }

        /// <summary>
        /// Порядок сортировки строк.
        /// </summary>
        public SortOrder SortOrderRows { get; set; } = SortOrder.Ascending;

        public event EventHandler<FindEventArgs> FindedRowIndexChanged = (sender, args) => { };

        

        public TableViewModel(IEnumerable<object> collection, Action changeCallBackAction, Action sortActionCallBack, Action searchActioncallBack, bool showNameHeaderByAttribute = true)
        {
            
            _changeCallBackAction += changeCallBackAction;
            _rowCompareFunc = Comparer;
            _sortAction += sortActionCallBack;
            _sortAction += QuickOrderRows;
            _searchAction += searchActioncallBack;
            _searchAction += () => { FindedRowIndexChanged.Invoke(this, new FindEventArgs(SearchingRow())); };
            _showNameHeaderByAttribute = showNameHeaderByAttribute;
            DataSource = collection;
        }

        private int Comparer(Object obj1, Object obj2)
        {
            return CompareByFields(obj1, obj2);
        }

        private int CompareByFields(Object obj1, Object obj2, int indexProperty = 0)
        {
            if (_sortPropertiesPriority?[indexProperty] == null) throw new ArgumentNullException(nameof(_sortPropertiesPriority));

            var sortProperty = _sortPropertiesPriority[indexProperty];

            var value1 = sortProperty.GetValue(obj1, null).ToString();
            var value2 = sortProperty.GetValue(obj2, null).ToString();

            int resultCompare = String.Compare(value1, value2);

            if (resultCompare == 0 && ++indexProperty < _sortPropertiesPriority.Count)
            {
                resultCompare = CompareByFields(obj1, obj2, indexProperty);
            }

            return resultCompare;

        }

        /// <summary>
        /// Быстрая сортировка с учетом приоритета колонок.
        /// </summary>
        private void QuickOrderRows()
        {
            StartOperation(() =>
            {
                _sortPropertiesPriority = new List<PropertyInfo>();
                foreach (
                    var column in
                        Columns.OrderByDescending(p => p.SortPriority)
                            .Select(p => ((Binding)p.DisplayMemberBinding).Path.Path))
                {
                    if (Rows.Count > 0)
                        _sortPropertiesPriority.Add(Rows[0].GetType()
                            .GetProperty(column, BindingFlags.Instance | BindingFlags.Public));
                }

                _rows =
                    new ObservableCollectionWithMaxLenElement<object>(QuickSort<Object>.StartSort(Rows.ToArray(),
                        SortOrderRows, _rowCompareFunc));

                StartOperation(CreateHashTableCollection, ()=> {});

            }, _changeCallBackAction);
            
        }

        private void CreateHashTableCollection()
        {
            if (!Rows.Any()) return;
            
            _hashTableFields =new Dictionary<string, HashTableWithResolveCollisions<long, int>>();
            var properties = CustomReflection.GetPropertiesByObject(Rows.FirstOrDefault());

            foreach (var property in properties)
            {
                HashTableWithResolveCollisions<long, int> hashTableForField = new HashTableWithResolveCollisions<long, int>();
                
                for(int countRow=0;countRow<Rows.Count;countRow++)
                {
                    hashTableForField.Add(HashRs(property.GetValue(Rows[countRow], null).ToString()), countRow);
                }
                _hashTableFields.Add(property.Name, hashTableForField);
            }
        }

        private IEnumerable<int> _searchResult;
        private int _indexSearchResult=-1;

        public int SearchingRow()
        {
            int index = 0;
            if (_searchResult != null && _searchResult.Any())
            {
                index = _searchResult.ElementAt(++_indexSearchResult);
            }
            else
            {
                index = SearchRows(SearchRowsByColumn);
                if (index < 0)
                {
                    index = SearchRows(SearchRowsSubstringByColumn);
                    
                }
            }
            return index;
        }

        private int SearchRows(Func<GridViewColumnDescription, List<int>> searchByColumn)
        {
            int index = 0;
                IEnumerable<int> findedRowsByAllColumns = new List<int>();
                foreach (var column in Columns)
                {
                    if (column.SearchParametr?.Length > 0)
                    {
                        List<int> findedRows = searchByColumn(column);
                        findedRowsByAllColumns = findedRowsByAllColumns.Any()
                            ? findedRowsByAllColumns.Intersect(findedRows)
                            : findedRows;
                    }
                }
                _searchResult = findedRowsByAllColumns;
                _indexSearchResult = -1;
                index = _searchResult.Any() ? _searchResult.ElementAt(++_indexSearchResult) : -1;
            
            return index; //todo return findedRowsBy...
        }

        private List<int> SearchRowsByColumn(GridViewColumnDescription column)
        {
            HashTableWithResolveCollisions<long, int> hashTable;
            List<int> value = new List<int>();
            if (_hashTableFields!=null && _hashTableFields.TryGetValue(column.GetMemberBindingName, out hashTable))
            {
                hashTable.TryGetValue(HashRs(column.SearchParametr), out value);
            }
            return value;
        }

        private List<int> SearchRowsSubstringByColumn(GridViewColumnDescription column)
        {
            List<int> resultList = new List<int>();

            if (!String.IsNullOrEmpty(column.SearchParametr))
            {
                var property = CustomReflection.GetPropertiesByObject(Rows.FirstOrDefault()).FirstOrDefault(p => p.Name == column.GetMemberBindingName);
                
                if (property != null)
                {
                    for(int i=0;i<Rows.Count;i++)
                    {
                        if (SearchingKMP.KMP(column.SearchParametr, property.GetValue(Rows[i], null).ToString()))
                        {
                            resultList.Add(i);
                        }
                    }
                }
            }
            return resultList;
        }

        private List<int> searchtest(GridViewColumnDescription column)
        {
            HashTableWithResolveCollisions<long, int> hashTable;
            List<int> value = new List<int>();
            if (_hashTableSubstringFields != null && _hashTableSubstringFields.TryGetValue(column.GetMemberBindingName, out hashTable))
            {
                hashTable.TryGetValue(HashRs(column.SearchParametr), out value);
            }
            return value;
        }

        //private void HashingSubstrings()
        //{
        //    if (!Rows.Any()) return;

        //    _hashTableSubstringFields = new Dictionary<string, HashTableWithResolveCollisions<long, int>>();
        //    var properties = CustomReflection.GetPropertiesByObject(Rows.FirstOrDefault());

        //    foreach (var property in properties)
        //    {
        //        HashTableWithResolveCollisions<long, int> hashTableForField = new HashTableWithResolveCollisions<long, int>();

        //        for (int countRow = 0; countRow < Rows.Count; countRow++)
        //        {
        //            var listSubstring = PartitionWord.PartitionOnSubstring(property.GetValue(Rows[countRow], null).ToString());

        //            foreach (var substring in listSubstring)
        //            {
        //                hashTableForField.Add(HashRs(substring), countRow);
        //            }
        //        }
        //        _hashTableSubstringFields.Add(property.Name, hashTableForField);
        //    }
        //}

        private long HashRs(string str)
        {

            const int b = 378551;
            int a = 63689;
            long hash = 0;
            foreach (var ch in str)
            {
                hash = hash*a+(int)Char.GetNumericValue(ch);
                a *= b;
            }
            return hash;
        }

       

    /// <summary>
    /// Запуск действия в новом потоке.
    /// </summary>
    /// <param name="workAction"></param>
    /// <param name="afterWorkAction"></param>
    private void StartOperation(Action workAction, Action afterWorkAction)
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += (sender, args) => workAction();
            worker.RunWorkerCompleted += (sender, args) => afterWorkAction();
            worker.RunWorkerAsync();
        }

        //todo delete
        /// <summary>
        /// Сортировка строк с учетом приоритета колонок.
        /// Примечание: работает медленней чем быстрая сортировка.
        /// </summary>
        private void ExpressionTreeOrderRows()
        {
            _rows = new ObservableCollectionWithMaxLenElement<object>(Rows.ExpressionTreeOrderWithPriority(Columns.Cast<GridViewColumn>(), SortOrderRows));
            _changeCallBackAction();
        }

        private void FilterRows()
        {
            
        }

        
        
    }
    
}
