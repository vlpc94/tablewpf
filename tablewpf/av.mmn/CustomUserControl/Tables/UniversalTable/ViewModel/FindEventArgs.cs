﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomUserControl.Tables.UniversalTable
{
    public class FindEventArgs:EventArgs
    {
        public int FindedRowIndex;

        public FindEventArgs(int findedRowIndex)
        {
            FindedRowIndex = findedRowIndex;
        }
    }
}
