﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomControls
{
    public struct ChangeableKeyValuePair<TKey, TValue>
    {
        public TKey Key { get; }
        public TValue Value { get; set; }

        public ChangeableKeyValuePair(TKey key, TValue value)
        {
            Key = key;
            Value = value;
        }
    }
}
